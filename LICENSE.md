# Prix carburants

By [Benoît Courty](mailto:benoit.courty@assemblee-nationale.fr),
By [Chloé Lallemand](mailto:chloe.lallemand@assemblee-nationale.fr),
By Kendrick Herberg

Copyright (C) 2022, 2023 Assemblée nationale

https://git.leximpact.dev/leximpact/simulateur-socio-fiscal/openfisca/prix-carburants/

> Prix carburants LexImpact is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> Prix carburants LexImpact is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.

